# HRI SOFTWARE 

This package contains instructions for installing and using software useful to develop HRI applications.

## INSTALL 

### OPTION 1 - DOCKER INSTALLATION 

* See [docker/README](docker/README.md)


### OPTION 2 - FULL LINUX INSTALLATION 

* See [scripts/README](scripts/README.md)



## TEST

Run each set of commands in a different terminal.
If you are using `tmux` in docker (default configuration), 
use `CTRL-b c` to create new tmux windows.

### ROS

* Run Stage simulation

        cd $MARRTINO_APPS_HOME/stage
        roslaunch simrobot.launch

    You should see simulation window with a blue robot.

    To run other simulated environments

        roscd stage_environments/scripts
        ./start_simulation.py -h             # prints available options
        ./start_simulation.py --list         # shows a list of available maps
        ./start_simulation.py <map_name>     # starts simulation with specified map

    To quit simulated environments

        roscd stage_environments/scripts
        ./quit.sh

    
* Move the robot

        cd $MARRTINO_APPS_HOME/program
        python robot_program_1.py

    You should see the robot moving and coming back to its original position.


### NAOqi

* Run NAOqi

        cd /opt/Aldebaran/naoqi-sdk-2.5.5.5-linux64
        ./naoqi

* Run `pepper_tools` scripts

        cd ~/src/pepper_tools/memory
        python read.py

        cd ~/src/pepper_tools/say
        python say.py --sentence "Hello"




## Development

Use `playground` folder to write programs for your robot. Do not directly edit files in 
`marrtino_apps` or `pepper_tools`, as it will prevent future updates.


### ROS

* Host OS

        cd ~/playground
        <your_preferred_editor> moverobot.py

            import os, sys

            sys.path.append(os.getenv('MARRTINO_APPS_HOME')+'/program')

            import robot_cmd_ros
            from robot_cmd_ros import *

            begin()

            forward(1)
            turn(180)
            forward(1)
            turn(180)

            end()



* Docker container

    Start a simulated environment and then the run the script

        cd ~/playground
        python moverobot.py


### NAOqi


* Host OS

        cd ~/playground
        <your_preferred_editor> sayhello.py

            import os, sys

            sys.path.append(os.getenv('PEPPER_TOOLS_HOME')+'/cmd_server')

            import pepper_cmd
            from pepper_cmd import *

            begin()

            pepper_cmd.robot.say('Hello')

            end()



* Docker container

    Terminal 1:

        cd /opt/Aldebaran/naoqi-sdk-2.5.5.5-linux64
        ./naoqi

    Terminal 2:

        cd ~/playground
        python sayhello.py







